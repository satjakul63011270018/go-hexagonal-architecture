package service

import (
	"go-Hexagonal-Architecture/repository"
)
type productService struct {
    productRepo repository.ProductRepository
}

func NewProductService(productRepo repository.ProductRepository) productService {
    return productService{productRepo: productRepo}
}

func (s productService) GetProduct() ([]ProductResponse, error) {
    products, err := s.productRepo.GetAll()
    if err != nil {
        return nil, err
    }
    var response []ProductResponse
    for _, p := range products {
        response = append(response, ProductResponse{
            ID:            p.ID,
            ProductName:   p.ProductName,
            Price:         p.Price,
            ProductDetail: p.ProductDetail,
        })
    }
    return response, nil
}

func (s productService) GetProductById(id int) (*ProductResponse, error) {
    product, err := s.productRepo.GetById(id)
    if err != nil {
        return nil, err
    }
    if product == nil {
        return nil, nil 
    }

    response := &ProductResponse{
        ID:            product.ID,
        ProductName:   product.ProductName,
        Price:         product.Price,
        ProductDetail: product.ProductDetail,
    }
    return response, nil
}

func (s productService) NewProduct(req ProductResponse) (*ProductResponse ,error) {
    product := repository.Product{
        ProductName: req.ProductName,
        Price: req.Price,
        ProductDetail: req.ProductDetail,
    }


    newproduct , err := s.productRepo.Create(product)
    if err != nil {
        return nil, err
    }

    response := ProductResponse{
        ID:            newproduct.ID,
        ProductName:   newproduct.ProductName,
        Price:         newproduct.Price,
        ProductDetail: newproduct.ProductDetail,
    }

    return &response , nil
}

func (s productService) UpdateProduct(id int, req ProductResponse) (*ProductResponse ,error) {
    product := repository.Product{
        ProductName: req.ProductName,
        Price: req.Price,
        ProductDetail: req.ProductDetail,
    }

    newproduct , err := s.productRepo.Update(id, product)
    if err != nil {
        return nil, err
    }

    response := ProductResponse{
        ID:            newproduct.ID,
        ProductName:   newproduct.ProductName,
        Price:         newproduct.Price,
        ProductDetail: newproduct.ProductDetail,
    }

    return &response , nil
}

func (s productService) DeleteProduct(id int) (error) {
    err := s.productRepo.Delete(id)
    if err != nil {
        return err
    }
    
    return  nil
}




