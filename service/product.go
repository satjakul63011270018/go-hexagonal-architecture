package service

type ProductResponse struct {
	ID            int       `json:"id"`
	ProductName   string    `json:"product_name"`
	Price         float64   `json:"price"` 
	ProductDetail string    `json:"product_detail"`
}


type ProductService interface {
		GetProduct() ([]ProductResponse , error)
		GetProductById(int) (*ProductResponse ,error)
		NewProduct(ProductResponse) (*ProductResponse ,error)
		UpdateProduct(int, ProductResponse) (*ProductResponse ,error)
		DeleteProduct(int) (error)
}