package repository

type Product struct {
	ID            int       `db:"id"`
	ProductName   string    `db:"product_name"`
	Price         float64   `db:"price"` 
	ProductDetail string    `db:"product_detail"`
	DateCreated   string 		`db:"date_created"`
}


type ProductRepository interface {
	GetAll() ([]Product, error)
	GetById(id int) (*Product, error)
	Create(Product) (*Product, error)
	Update(int, Product) (*Product, error)
	Delete(id int) (error)
}