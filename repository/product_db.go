package repository

import (
	"database/sql"
)

type productRepositoryDB struct {
	db *sql.DB
}

func NewProductRepositoryDB(db *sql.DB) productRepositoryDB {
	return productRepositoryDB{db: db}
}

func (r productRepositoryDB) GetAll() ([]Product, error) {
	var products []Product
	query := `SELECT id, product_name, price, product_detail, date_created FROM product`
	rows, err := r.db.Query(query)
	if err != nil {
			return nil, err
	}
	defer rows.Close()

	for rows.Next() {
			var p Product
			err := rows.Scan(&p.ID, &p.ProductName, &p.Price, &p.ProductDetail, &p.DateCreated)
			if err != nil {
					return nil, err
			}
			products = append(products, p)
	}

	if err = rows.Err(); err != nil {
			return nil, err
	}
	
	return products, nil
}

func (r productRepositoryDB) GetById(id int) (*Product, error) {
	query := `SELECT id, product_name, price, product_detail, date_created FROM product WHERE id = $1`
	row := r.db.QueryRow(query, id)

	var p Product
	err := row.Scan(&p.ID, &p.ProductName, &p.Price, &p.ProductDetail, &p.DateCreated)
	if err != nil {
			if err == sql.ErrNoRows {
					return nil, nil
			}
			return nil, err
	}

	return &p, nil
}

func (r productRepositoryDB) Create(prod Product) (*Product, error) {
	query := `INSERT INTO Product (product_name, price, product_detail) VALUES ($1, $2, $3) RETURNING id;`
	var id int
	err := r.db.QueryRow(
			query,
			prod.ProductName,
			prod.Price,
			prod.ProductDetail,
	).Scan(&id)

	if err != nil {
			return nil, err
	}

	prod.ID = id

	return &prod, nil
}

func (r productRepositoryDB) Update(prodID int, prod Product) (*Product, error) {
	query := `UPDATE Product SET product_name = $1, price = $2, product_detail = $3 WHERE id = $4;`
	_, err := r.db.Exec(query, prod.ProductName, prod.Price, prod.ProductDetail, prodID)
	if err != nil {
			return nil, err
	}

	prod.ID = prodID

	return &prod, nil
}

func (r productRepositoryDB) Delete(id int) error {
	query := `DELETE FROM Product WHERE id = $1;`

	result, err := r.db.Exec(query, id)
	if err != nil {
			return err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
			return err
	}
	if rowsAffected == 0 {
			return sql.ErrNoRows
	}

	return nil
}




