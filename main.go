package main

import (
	"database/sql"
	"fmt"
	"go-Hexagonal-Architecture/handler"
	"go-Hexagonal-Architecture/repository"
	"go-Hexagonal-Architecture/service"
	"log"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	_ "github.com/lib/pq"
)

const (
    host         = "localhost"
    port         = 5432
    databaseName = "postgres"
    username     = "postgres"
    password     = "satjakul"
)

func main() {

    psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
        "password=%s dbname=%s sslmode=disable",
        host, port, username, password, databaseName)

    db, err := sql.Open("postgres", psqlInfo)
    if err != nil {
        log.Fatal(err)
    }
				
    // defer db.Close()


    // err = db.Ping()
    // if err != nil {
    //     log.Fatal(err)
    // }
		

    // fmt.Println("Successfully connected!")

		productRepository := repository.NewProductRepositoryDB(db)
		productService := service.NewProductService(productRepository)
		productHandler := handler.NewProductHandler(productService)

		_ = productService

		app := fiber.New()

		app.Use(cors.New(cors.Config{
			AllowOrigins: "http://127.0.0.1:300", 
			AllowMethods: "GET,POST,PUT,DELETE",
			AllowHeaders: "Origin, Content-Type, Accept",
		}))

		app.Use(logger.New(logger.Config{
			Format:     "[${time}] ${status} - ${method} ${path}\n",
			TimeFormat: "02-Jan-2006 15:04:05",
			TimeZone:   "Local",
		}))
		
		productHandler.SetupRoutes(app)

		log.Fatal(app.Listen(":3000"))
		if err != nil {
			panic(err)
		}
}

