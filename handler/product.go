package handler

import (
	"database/sql"
	"go-Hexagonal-Architecture/service"
	"strconv"

	"github.com/gofiber/fiber/v2"
)

type productHandler struct {
	productSrv service.ProductService
}

func NewProductHandler(productSrv service.ProductService) *productHandler {
	return &productHandler{productSrv: productSrv}
}

func (h *productHandler) getAllProducts(c *fiber.Ctx) error {
	products, err := h.productSrv.GetProduct()
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(&fiber.Map{
			"error": "Internal Server Error",
		})
	}

	return c.Status(fiber.StatusOK).JSON(products)
}


func (h *productHandler) getProductByID(c *fiber.Ctx) error {
	id, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
			"error": "Invalid Product ID",
		})
	}

	product, err := h.productSrv.GetProductById(id)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(&fiber.Map{
			"error": "Internal Server Error",
		})
	}
	if product == nil {
		return c.Status(fiber.StatusNotFound).JSON(&fiber.Map{
			"error": "Product Not Found",
		})
	}

	return c.Status(fiber.StatusOK).JSON(product)
}

func (h *productHandler) NewProduct(c *fiber.Ctx) error {
	var req service.ProductResponse
	if c.Get("Content-Type") != fiber.MIMEApplicationJSON {
			return c.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
					"error": "Content type must be application/json",
			})
	}
	
	if err := c.BodyParser(&req); err != nil {
			return c.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
					"error": "Bad Request - JSON parsing error",
			})
	}

	response, err := h.productSrv.NewProduct(req)
	if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(&fiber.Map{
					"error": "Internal Server Error",
			})
	}

	return c.Status(fiber.StatusCreated).JSON(response)
}

func (h *productHandler) UpdateProduct(c *fiber.Ctx) error {
	id, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
			"error": "Invalid product ID",
		})
	}

	var req service.ProductResponse
	if err := c.BodyParser(&req); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
			"error": "Bad Request - JSON parsing error",
		})
	}

	response, err := h.productSrv.UpdateProduct(id, req)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(&fiber.Map{
			"error": "Internal Server Error",
		})
	}

	return c.Status(fiber.StatusOK).JSON(response)
}

func (h *productHandler) DeleteProduct(c *fiber.Ctx) error {
	id, err := strconv.Atoi(c.Params("id"))
	if err != nil {
			return c.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
					"error": "Invalid Product ID",
			})
	}

	err = h.productSrv.DeleteProduct(id)
	if err != nil {
			if err == sql.ErrNoRows {
					return c.Status(fiber.StatusNotFound).JSON(&fiber.Map{
							"error": "Product Not Found",
					})
			}
			return c.Status(fiber.StatusInternalServerError).JSON(&fiber.Map{
					"error": "Internal Server Error",
			})
	}

	return c.SendStatus(fiber.StatusNoContent)
}

func (h *productHandler) SetupRoutes(app *fiber.App) {
	app.Get("/products", h.getAllProducts)
	app.Get("/products/:id", h.getProductByID)
	app.Post("/products", h.NewProduct)
	app.Put("/products/:id", h.UpdateProduct)
	app.Delete("/products/:id", h.DeleteProduct)
}
